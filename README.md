# Heaven and Hearts Shop
A front-end ecommerce app made in Vue with a serverless (Lambda) backend function for processing payments. Deployed through Netlify.

## TODOS:
[x] Pass meta-data through to Stripe to collect year of penny requested.
[x] Add form field to enter email address, name and shipping address
[x] Add details page for more images information

[x] Add receipt email
[x] "Added to cart" confirmation w/ link to checkout

[x] Preserve state in localStorage
[x] Loading spinner and disable form on checkout
[x] Clear cart on successful charge
[x] Validation on all form inputs
[x] Show indication of success

[x] Add Google Analytics
[ ] Show indication of Stripe errors

[x] Add shipping calculations ($5 flat fee, free $50 or more)
[x] Add additional shipping calculation $2 for 1 item, $3 for 2, $4 for 3, $5 for 4, Free for $50 or more.
[x] Mobile friendly views

[x] Add navigation [about, contact/support]
[x] hook up support form OR redirect /support to the Facebook Page
[ ] Add About page
[ ] Send Stacey message on Successful Order (email, facebook, sms?)
[ ] Add Unit tests
[ ] Add e2e tests
[ ] Add form for custom orders/quote

<!-- [ ] Can I change the Stripe receipt email template? -->

## Backlog: 
- Add Venmo payment option
- Add PayPal payment option
- add Firebase CloudStore backend for data persistence
- add Firebase Authentication
- create backend for managing products
