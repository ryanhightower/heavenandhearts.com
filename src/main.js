import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "./registerServiceWorker";
import Buefy from "buefy";
import "buefy/dist/buefy.css";
import VTooltip from "v-tooltip";
import VueAnalytics from "vue-analytics";

Vue.use(VueAnalytics, {
  id: "UA-27959341-5",
  router
});

Vue.use(VTooltip);
Vue.use(Buefy, {
  defaultIconPack: "fas"
});

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
